/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minisql;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author djwal
 */
public class AnalizadorSintactico {
   public ArrayList <String> receptor = new ArrayList();
    public ArrayList <String> receptorErrores = new ArrayList();
    public int posicionInicial = 0;
    public String temporal, msgValorErroneo = "";
    
    
    public void parsing(ArrayList<String> tomaToken){
        receptor = tomaToken;
        temporal = receptor.get(posicionInicial);
        inicioStatement();
    }
    
    public void inicioStatement(){
        switch(temporal){
            case "SELECT":
                SELECT();
                break;
            case "CREATE":
                CREATE();
                break;
            case "INSERT":
                INSERT();
                break;
            case "ALTER":
                ALTER();
                break;
            case "UPDATE":
                UPDATE();
                break;
            case "DELETE":
                DELETE();
                break;
            case "TRUNCATE":
                TRUNCATE();
                break;
            case "DROP":
                DROP();
                break;
            default:
                ERROR();
                break;
        }
    }
    
    public void validarArchivo(String palabra){
        if ((temporal == palabra) && posicionInicial < receptor.size()-2 ) {
            posicionInicial = posicionInicial + 2;
            temporal = receptor.get(posicionInicial);
            return;
        }
        
         if ((temporal == palabra) && posicionInicial == receptor.size()-2 ) {
            finalizarRecorrido();
        }else{
             ERROR();
         }
    }

    private void SELECT() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "SELECT") {
            comparador("SELECT");
            SELECT_2();
            if (temporal == "PYC") {
                comparador("PYC");
                inicioStatement();
            }
            if (temporal == "GO") {
                comparador("GO");
                inicioStatement();
            }else{
                ERROR();
            }
        }
    }

    private void CREATE() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void INSERT() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void ALTER() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void UPDATE() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void DELETE() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void TRUNCATE() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void DROP() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void ERROR() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        msgValorErroneo += temporal + " --> Error Sintáctico. Línea: " + receptor.get(posicionInicial+1) + "\n";
        
        while((temporal != "PYC" && temporal != "GO") && (posicionInicial < receptor.size()-2)){
            posicionInicial = posicionInicial + 2;
            temporal = receptor.get(posicionInicial);
        }
        
        if ((temporal != "PYC" && temporal != "GO") && (posicionInicial == receptor.size()-2)) {
            msgValorErroneo += "    --> Error Sintactico.   Falta Símbolo Punto y coma o Instrucción GO. Línea: " + receptor.get(posicionInicial+1)+"\n";
        }
        
        if (posicionInicial != receptor.size()-2) {
            posicionInicial = posicionInicial + 2;
            temporal = receptor.get(posicionInicial);
            inicioStatement();
        }else{
            ERROR();
        }
    }

    private void comparador(String comparo) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if ((temporal == comparo) && (posicionInicial < receptor.size()-2)) {
            posicionInicial = posicionInicial + 2;
            temporal = receptor.get(posicionInicial);
            return;
            
        }
        if ((temporal == comparo) && (posicionInicial == receptor.size()-2)) {
            finalizarRecorrido();
        }else{
            ERROR();
        }
    }

    private void finalizarRecorrido() {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            if (temporal != "PYC" && temporal != "GO") {
                msgValorErroneo += "    -- Error: Falta sìmbolo punto y Coma o Instrucciòn GO. Linea " + receptor.get(posicionInicial+1)+ "\n";
            }
            msgValorErroneo += "FIN";
            escrituraSalidaSintact();
        } catch (IOException ex) {
            Logger.getLogger(AnalizadorSintacticoR.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void escrituraSalidaSintact() throws IOException{
        FileWriter archivo = new FileWriter("SalidaSintactico.out");
        //Logger.getLogger(AnalizadorSintactico.class.getName()).log(Level.SEVERE,null,ex);
        BufferedWriter buffer = new BufferedWriter(archivo);
        PrintWriter escribeArchivo = new PrintWriter(buffer);
        
        escribeArchivo.write(msgValorErroneo);
        escribeArchivo.close();
        buffer.close();
        
        System.out.println(" ***   Fin Sintactico *** ");
        System.exit(0);
    }

    private void SELECT_2() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        switch(temporal){
            case "ALL":
                comparador("ALL");
                break;
            case "DISTINCT":
                comparador("DISTINCT");
                break;
            default:
                //no posee epsilon
                break;
        }
        SELECT_3();
    }

    private void SELECT_3() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "TOP") {
            comparador("TOP");
            SELECT_4();
        }else{
            SELECT_6();
        }
    }

    private void SELECT_4() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "ParentesisAbierto") {
            comparador("ParentesisAbierto");
            EXPRESSION_SELECT();
            comparador("ParentesisCerrado");
            SELECT_5();
        }else{
            ERROR();
        }
    }

    private void SELECT_6() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      SELECT_ASTERISCOIDENTI();
      SELECT_7();
    }    

    private void SELECT_5() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "Porcentaje") {
            comparador("Porcentaje");
            SELECT_6();
        }
    }

    private void SELECT_7() {
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "INTO") {
            comparador("INTO");
            SELECT_8();
        }else{
            SELECT_10();
        }
    }

    private void SELECT_8() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "IDENTIFICADOR") {
            comparador("IDENTIFICADOR");
            SELECT_9();
        }else{
            ERROR();
        }
    }

    private void SELECT_9() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "PUNTO") {
            comparador("PUNTO");
            comparador("IDENTIFICADOR");
        }else{
            SELECT_10();
        }
    }

    private void SELECT_10() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "FROM") {
            comparador("FROM");
            NOMBRETABLA();
            SELECT_16();
        }else{
            SELECT_11();
        }
    }

    private void SELECT_11() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "WHERE") {
            comparador("WHERE");
            CONDICION_WHERE();
            SELECT_12();   
        }else{
            SELECT_12();
        }
    }

    private void SELECT_12() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "GROUP") {
            comparador("GROUP");
            comparador("GROUP");
            comparador("BY");
            SALTO_SELECT();
            SELECT_13();
        }else{
            SELECT_13();
        }
    }

    private void SELECT_13() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "HAVING") {
            comparador("HAVING");
            CONTINUACION_HAVING();
            CONDICION_WHERE();
            SELECT_14();
        }else{
            SELECT_14();
        }
    }

    private void SELECT_14() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (temporal == "ORDER") {
            comparador("ORDER");
            comparador("BY");
            ORDER();
        }else{
            //Epsilon
            //Tomar en cuenta que no hay SELECT16
        }
    }

    private void EXPRESSION_SELECT() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_4
    }

    private void SELECT_ASTERISCOIDENTI() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_6

    }

    private void NOMBRETABLA() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_10

    }

    private void SELECT_16() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_10

    }

    private void CONDICION_WHERE() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_11

    }

    private void SALTO_SELECT() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_12

    }

    private void CONTINUACION_HAVING() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_13

    }

    private void ORDER() {
        throw new UnsupportedOperationException("Not supported yet."); 
        //Pendiente de definir... viene de SELECT_14

    }
    
    
    
    
}
