package minisql;
import static minisql.Tokens.*;
%%
%class Lexer
%type Tokens
%line
%column
IDENTIFICADOR7 = [a-zA-Z]([a-zA-Z]|[0-9]|[_]|"ñ"|"Ñ")*
%{
        public String lexeme;
        public int linea;
        public int columna;
%}

%%

[ \n\t\r]+  {/* Ignorar Espacios  */}

            /*****                  PALABRAS RESERVADAS             *****/

"ABSOLUTE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ABSOLUTE;}
"ACTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ACTION;}
"ADA"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ADA;}
"ADD"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ADD;}
"ALL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ALL;}
"ALLOCATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ALLOCATE;}
"ALTER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ALTER;}
"AND"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return AND;}
"ANY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ANY;}
"ARE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ARE;}
"AS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return AS;}
"ASC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ASC;}
"ASSERTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ASSERTION;}
"AT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return AT;}
"AUTHORIZATION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return AUTHORIZATION;}
"AVG"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return AVG;}
"BEGIN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BEGIN;}
"BETWEEN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BETWEEN;}
"BIT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BIT;}
"BIT_LENGTH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BIT_LENGTH;}
"BOTH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BOTH;}
"BY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BY;}
"CASCADE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CASCADE;}
"CASCADED"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CASCADED;}
"CASE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CASE;}
"CAST"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CAST;}
"CATALOG"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CATALOG;}
"CHAR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CHAR;}
"CHAR_LENGTH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CHAR_LENGTH;}
"CHARACTER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CHARACTER;}
"CHARACTER_LENGTH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CHARACTER_LENGTH;}
"CHECK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CHECK;}
"CLOSE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CLOSE;}
"COALESCE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COALESCE;}
"COLLATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COLLATE;}
"COLLATION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COLLATION;}
"COLUMN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COLUMN;}
"COMMIT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COMMIT;}
"CONNECT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONNECT;}
"CONNECTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONNECTION;}
"CONSTRAINT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONSTRAINT;}
"CONSTRAINTS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONSTRAINTS;}
"CONTINUE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONTINUE;}
"CONVERT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONVERT;}
"CORRESPONDING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CORRESPONDING;}
"COUNT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COUNT;}
"CREATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CREATE;}
"CROSS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CROSS;}
"CURRENT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CURRENT;}
"CURRENT_DATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CURRENT_DATE;}
"CURRENT_TIME"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CURRENT_TIME;}
"CURRENT_TIMESTAMP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CURRENT_TIMESTAMP;}
"CURRENT_USER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CURRENT_USER;}
"CURSOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CURSOR;}
"DATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DATE;}
"DAY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DAY;}
"DEALLOCATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DEALLOCATE;}
"DEC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DEC;}
"DECIMAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DECIMAL;}
"DECLARE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DECLARE;}
"DEFAULT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DEFAULT;}
"DEFERRABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DEFERRABLE;}
"DEFERRED"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DEFERRED;}
"DELAYED_DURABILITY" {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DELAYED_DURABILITY;}
"DELETE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DELETE;}
"DESC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DESC;}
"DESCRIBE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DESCRIBE;}
"DESCRIPTOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DESCRIPTOR;}
"DIAGNOSTICS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DIAGNOSTICS;}
"DISCONNECT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DISCONNECT;}
"DISTINCT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DISTINCT;}
"DOMAIN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DOMAIN;}
"DOUBLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DOUBLE;}
"DROP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DROP;}
"ELSE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ELSE;}
"END"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return END;}
"END-EXEC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return END_EXEC;}
"ESCAPE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ESCAPE;}
"EXCEPT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXCEPT;}
"EXCEPTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXCEPTION;}
"EXEC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXEC;}
"EXECUTE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXECUTE;}
"EXISTS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXISTS;}
"EXTERNAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXTERNAL;}
"EXTRACT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXTRACT;}
"FALSE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FALSE;}
"FETCH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FETCH;}
"FIRST"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FIRST;}
"FLOAT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FLOAT;}
"FOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FOR;}
"FOREIGN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FOREIGN;}
"FORTRAN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FORTRAN;}
"FOUND"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FOUND;}
"FROM"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FROM;}
"FULL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FULL;}
"GET"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return GET;}
"GLOBAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return GLOBAL;}
"GO"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return GO;}
"GOTO"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return GOTO;}
"GRANT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return GRANT;}
"GROUP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return GROUP;}
"HAVING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return HAVING;}
"HOUR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return HOUR;}
"IDENTITY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IDENTITY;}
"IMMEDIATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IMMEDIATE;}
"IN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IN;}
"INCLUDE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INCLUDE;}
"INDEX"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INDEX;}
"INDICATOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INDICATOR;}
"INITIALLY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INITIALLY;}
"INNER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INNER;}
"INPUT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INPUT;}
"INSENSITIVE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INSENSITIVE;}
"INSERT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INSERT;}
"INT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INT;}
"INTEGER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INTEGER;}
"INTERSECT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INTERSECT;}
"INTERVAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INTERVAL;}
"INTO"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return INTO;}
"IS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IS;}
"ISOLATION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ISOLATION;}
"JOIN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return JOIN;}
"KEY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return KEY;}
"LANGUAGE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LANGUAGE;}
"LAST"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LAST;}
"LEADING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LEADING;}
"LEFT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LEFT;}
"LEVEL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LEVEL;}
"LIKE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LIKE;}
"LOCAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LOCAL;}
"LOWER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LOWER;}
"MATCH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MATCH;}
"MAX"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MAX;}
"MIN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MIN;}
"MINUTE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MINUTE;}
"MODULE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MODULE;}
"MONTH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MONTH;}
"NAMES"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NAMES;}
"NATIONAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NATIONAL;}
"NATURAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NATURAL;}
"NCHAR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NCHAR;}
"NEXT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NEXT;}
"NO"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NO;}
"NONE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NONE;}
"NOT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NOT;}
"NULL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NULL;}
"NULLIF"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NULLIF;}
"NUMERIC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NUMERIC;}
"OCTET_LENGTH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OCTET_LENGTH;}
"OF"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OF;}
"ON"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ON;}
"ONLY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ONLY;}
"OPEN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPEN;}
"OPTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPTION;}
"OR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OR;}
"ORDER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ORDER;}
"OUTER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OUTER;}
"OUTPUT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OUTPUT;}
"OVERLAPS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OVERLAPS;}
"PAD"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PAD;}
"PARTIAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PARTIAL;}
"PASCAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PASCAL;}
"POSITION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return POSITION;}
"PRECISION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PRECISION;}
"PREPARE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PREPARE;}
"PRESERVE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PRESERVE;}
"PRIMARY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PRIMARY;}
"PRIOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PRIOR;}
"PRIVILEGES"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PRIVILEGES;}
"PROCEDURE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PROCEDURE;}
"PUBLIC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PUBLIC;}
"READ"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return READ;}
"REAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return REAL;}
"REFERENCES"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return REFERENCES;}
"RELATIVE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RELATIVE;}
"RESTRICT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RESTRICT;}
"REVOKE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return REVOKE;}
"RIGHT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RIGHT;}
"ROLLBACK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ROLLBACK;}
"ROWS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ROWS;}
"SCHEMA"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SCHEMA;}
"SCROLL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SCROLL;}
"SECOND"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SECOND;}
"SECTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SECTION;}
"SELECT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SELECT;}
"SESSION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SESSION;}
"SESSION_USER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SESSION_USER;}
"SET"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SET;}
"SIZE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SIZE;}
"SMALLINT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SMALLINT;}
"SOME"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SOME;}
"SPACE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SPACE;}
"SQL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SQL;}
"SQLCA"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SQLCA;}
"SQLCODE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SQLCODE;}
"SQLERROR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SQLERROR;}
"SQLSTATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SQLSTATE;}
"SQLWARNING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SQLWARNING;}
"SUBSTRING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SUBSTRING;}
"SUM"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SUM;}
"SYSTEM_USER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SYSTEM_USER;}
"TABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TABLE;}
"TEMPORARY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TEMPORARY;}
"THEN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return THEN;}
"TIME"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TIME;}
"TIMESTAMP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TIMESTAMP;}
"TIMEZONE_HOUR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TIMEZONE_HOUR;}
"TIMEZONE_MINUTE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TIMEZONE_MINUTE;}
"TO"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TO;}
"TRAILING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRAILING;}
"TRANSACTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRANSACTION;}
"TRANSLATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRANSLATE;}
"TRANSLATION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRANSLATION;}
"TRIM"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRIM;}
"TRUE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRUE;}
"UNION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UNION;}
"UNIQUE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UNIQUE;}
"UNKNOWN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UNKNOWN;}
"UPDATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UPDATE;}
"UPPER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UPPER;}
"USAGE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return USAGE;}
"USER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return USER;}
"USING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return USING;}
"VALUE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return VALUE;}
"VALUES"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return VALUES;}
"VARCHAR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return VARCHAR;}
"VARYING"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return VARYING;}
"VIEW"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return VIEW;}
"WHEN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WHEN;}
"WHENEVER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WHENEVER;}
"WHERE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WHERE;}
"WITH"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WITH;}
"WORK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WORK;}
"WRITE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WRITE;}
"YEAR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return YEAR;}
"ZONE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ZONE;}
"BACKUP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BACKUP;}
"BREAK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BREAK;}
"BROWSE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BROWSE;}
"BULK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return BULK;}
"CHECKPOINT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CHECKPOINT;}
"CLUSTERED"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CLUSTERED;}
"COMPUTE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return COMPUTE;}
"CONTAINS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONTAINS;}
"CONTAINSTABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return CONTAINSTABLE;}
"DATABASE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DATABASE;}
"DBCC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DBCC;}
"DENY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DENY;}
"DISK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DISK;}
"DISTRIBUTED"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DISTRIBUTED;}
"DUMP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DUMP;}
"ERRLVL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ERRLVL;}
"EXIT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return EXIT;}
"FILE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FILE;}
"FILLFACTOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FILLFACTOR;}
"FREETEXT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FREETEXT;}
"FREETEXTTABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FREETEXTTABLE;}
"FUNCTION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FUNCTION;}
"HOLDLOCK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return HOLDLOCK;}
"IDENTITY_INSERT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IDENTITY_INSERT;}
"IDENTITYCOL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IDENTITYCOL;}
"IF"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return IF;}
"KILL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return KILL;}
"LINENO"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LINENO;}
"LOAD"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return LOAD;}
"MERGE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return MERGE;}
"NOCHECK"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NOCHECK;}
"NONCLUSTERED"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return NONCLUSTERED;}
"OFF"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OFF;}
"OFFSETS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OFFSETS;}
"OPENDATASOURCE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPENDATASOURCE;}
"OPENQUERY"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPENQUERY;}
"OPENROWSET"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPENROWSET;}
"OPENXML"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPENXML;}
"OVER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OVER;}
"PERCENT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PERCENT;}
"PIVOT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PIVOT;}
"PLAN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PLAN;}
"PRINT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PRINT;}
"PROC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return PROC;}
"RAISERROR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RAISERROR;}
"READTEXT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return READTEXT;}
"RECONFIGURE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RECONFIGURE;}
"REPLICATION"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return REPLICATION;}
"RESTORE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RESTORE;}
"RETURN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RETURN;}
"REVERT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return REVERT;}
"ROWCOUNT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ROWCOUNT;}
"ROWGUIDCOL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return ROWGUIDCOL;}
"RULE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RULE;}
"SAVE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SAVE;}
"SECURITYAUDIT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SECURITYAUDIT;}
"SEMANTICKEYPHRASETABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SEMANTICKEYPHRASETABLE;}
"SEMANTICSIMILARITYDETAILSTABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SEMANTICSIMILARITYDETAILSTABLE;}
"SEMANTICSIMILARITYTABLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SEMANTICSIMILARITYTABLE;}
"SETUSER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SETUSER;}
"SHUTDOWN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SHUTDOWN;}
"STATISTICS"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return STATISTICS;}
"TABLESAMPLE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TABLESAMPLE;}
"TEXTSIZE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TEXTSIZE;}
"TOP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TOP;}
"TRAN"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRAN;}
"TRIGGER"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRIGGER;}
"TRUNCATE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRUNCATE;}
"TRY_CONVERT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TRY_CONVERT;}
"TSEQUAL"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TSEQUAL;}
"UNPIVOT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UNPIVOT;}
"UPDATETEXT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return UPDATETEXT;}
"USE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return USE;}
"WAITFOR"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WAITFOR;}
"WHILE"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WHILE;}
"WITHIN GROUP"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WITHIN_GROUP;}
"WRITETEXT"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return WRITETEXT;}
"RETURNS"        {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return RETURNS;}
"FORWARD_ONLY"  {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FORWARD_ONLY;}
"STATIC"    {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return STATIC;}
"KEYSET"    {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return KEYSET;}
"DYNAMIC"   {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return DYNAMIC;}
"FAST_FORWARD"  {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return FAST_FORWARD;}
"READ_ONLY" {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return READ_ONLY;}
"SCROLL_LOCKS"  {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return SCROLL_LOCKS;}
"OPTIMISTIC"    {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return OPTIMISTIC;}  
"TYPE_WARNING" {lexeme=yytext(); linea=yyline+1; columna=yycolumn;  return TYPE_WARNING;}


            /*****                  IDENTIFICADORES              *****/

{IDENTIFICADOR7}  {lexeme=yytext();  linea=yyline; columna=yycolumn; return Identificador;}


/*****                  OPERADORES Y SÍMBOLOS             *****/

"+"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Suma;}
"-"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Resta;}
"*"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Multiplicacion;}
"/"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Division;}
"%"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Porcentaje;}
"<"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Menor;}
"<="    {lexeme=yytext(); linea=yyline; columna=yycolumn; return MenorIgual;}
">"     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Mayor;}
">="    {lexeme=yytext(); linea=yyline; columna=yycolumn; return MayorIgual;}
"="     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Igual;}
"=="    {lexeme=yytext();  linea=yyline; columna=yycolumn; return Igual2; }
"!="    {lexeme=yytext();  linea=yyline; columna=yycolumn; return AdmiracionIgual;}
"&&"    {lexeme=yytext();  linea=yyline; columna=yycolumn; return Y;}
"||"    {lexeme=yytext();   linea=yyline; columna=yycolumn; return O;}
"!"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return Admiracion;}
";"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return PYC;}
","     {lexeme=yytext();  linea=yyline; columna=yycolumn; return Coma;}
"."     {lexeme=yytext();  linea=yyline; columna=yycolumn; return Punto;}
"["     {lexeme=yytext();  linea=yyline; columna=yycolumn; return CorcheteAbierto;}
"]"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return CorcheteCerrado;}
"("     {lexeme=yytext();  linea=yyline; columna=yycolumn;return ParentesisAbierto;}
")"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return ParentesisCerrado;}
"{"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return LlaveAbierta;}
"}"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return LlaveCerrada;}
"[]"    {lexeme=yytext();  linea=yyline; columna=yycolumn; return Corchetes;}
"()"    {lexeme=yytext();  linea=yyline; columna=yycolumn; return Parentesis;}
"{}"    {lexeme=yytext();  linea=yyline; columna=yycolumn; return Llaves;}
"@"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return Arroba;}
"#"     {lexeme=yytext();  linea=yyline; columna=yycolumn; return Numeral;}
"##"    {lexeme=yytext();  linea=yyline; columna=yycolumn; return DobleNumeral;}


/*****                  COMENTARIOS                  *****/

"/*" [^*] ~ "*/"  {/*Ignore*/}  /* COMENTARIO MULTILINEA */
"--" [^\r\n]+  {/*Ignore*/}   /* COMENTARIO DE UNA LINEA */

/* CONSTATES BOOLEANAS */
//{BOOLEANAS} {analizar=yytext(); linea=yyline; columna=yycolumn; return Constante_Booleana;}

//[0|1]         {lexeme=yytext(); linea=yyline; columna=yycolumn; return Booleano;}

/*****                  NUMEROS ENTEROS             *****/

[0-9]+      {lexeme=yytext(); linea=yyline; columna=yycolumn; return Digito;}

/*****                  NUMEROS DECIMALES             *****/

[0-9]+ "." ([0-9]+ | [0-9]+(E | e) ("+")? [0-9]+ | (E | e) ("+")? [0-9]+ )?     {lexeme=yytext(); linea=yyline; columna=yycolumn; return Decimales;}

/*****                  CADENAS (valores dentro de comilla simple)      *****/

['][^'\n]*[']       {lexeme=yytext(); linea=yyline; columna=yycolumn; return Cadena;}

/* ERROR */

 .  {return ERROR;}

