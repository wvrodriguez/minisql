# MiniSQL

Proyecto del Curso de Compiladores 2019

La fase 1 del proyecto consiste en realizar un analizador lexico para SQL,
en el que debe validar todos las palabras reservadas del lenguaje, identificadores,
digitos, decimales, cadenas y comentarios.

Al iniciar, se debe buscar el archivo Lexer.jflex para generar el archivo 
Lexer.java, luego se debe cargar el archivo con extensión SQL para analizar su
gramática. Al finalizar la lectura del archivo, se muestran los tokens en la pantalla
y también se genera un archivo de salida con extensión OUT que tambien muestra el analisis 
general el archivo.

El manejo de errores se muestra en el mismo archivo indicando que el caracter no es valido,
en la fila y columna indicados. El programa tiene el alcance de ignorar comentarios y
espacios entre las palabras y simbolos.